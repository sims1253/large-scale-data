Well hello there

You should use Python 3!
I'd advise for the use of something like [pipenv](https://docs.pipenv.org/)
(or just virtualenv and pip seperate)

There is a `Pipfile` and `Pipfile.lock` in the project that contain all the
needed libraries, Pipfile.lock has working versions included, that you can use
to install all packages from with `Pipenv install` for the `Pipfile` or
`pipenv install --ignore-pipfile` for the `Pipfile.lock`.
Finally, I added coala as I like to run it on all my code.

For more smooth python coding, a proper IDE as eg.
[PyCharm](https://www.jetbrains.com/pycharm/download/#section=linux) can be a
great help.
